
from collections import defaultdict
from NetworkFlow import  *

def BFS(graph, s, t, parent):
    visited = {}
    for a_node in graph.nodes:
        visited[a_node] = False

    queue = []
    queue.append(s)
    visited[s] = True

    while queue:
        u = queue.pop(0)

        # Get all adjacent vertices of the dequeued vertex u
        # If a adjacent has not been visited, then mark it
        # visited and enqueue it
        for ind, val in enumerate(graph[u]):
            if visited[ind] == False and val > 0:
                queue.append(ind)
                visited[ind] = True
                parent[ind] = u

    return True if visited[t] else False



# Returns tne maximum flow from s to t in the given graph
def FordFulkerson(self, source, sink):
    parent = {}

    max_flow = 0  # There is no flow initially

    while self.BFS(source, sink, parent):

            # Find minimum residual capacity of the edges along the
            # path filled by BFS. Or we can say find the maximum flow
            # through the path found.
            path_flow = float("Inf")
            s = sink
            while (s != source):
                path_flow = min(path_flow, self.graph[parent[s]][s])
                s = parent[s]

            # Add path flow to overall flow
            max_flow += path_flow

            # update residual capacities of the edges and reverse edges
            # along the path
            v = sink
            while (v != source):
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]

        return max_flow



def run_ford_fulkerson():
    graphst = create_s_t_graph(get_multi_src_sink_graph())
    graphI = Graph(graphst)
    src = get_src(graphst)
    sink = get_sink(graphst)
    print(graphI.FordFulkerson(src, sink))