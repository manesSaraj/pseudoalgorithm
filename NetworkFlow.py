

import networkx
import matplotlib.pyplot as plt
import pylab
#import graphviz

class Node:
    def __init__(self, label, capacity):
        self.label = label
        self.capacity = capacity
        self.excess  = None

    def set_capacity(self, new_capacity):
        self.capacity = new_capacity

    def set_label(self, new_label):
        self.label = new_label

    def to_string(self):
        return "label:{0}, cpacity:{1}".format(self.label, self.capacity)

    def clone_self(self):
        return Node(self.label, self.capacity)

    def set_excess(self, value):
        self.excess = value



# Refer this in doubt
#https://stackoverflow.com/questions/20133479/how-to-draw-directed-graphs-using-networkx-in-python

def draw_a_graph(graph, val_map, figure, pos=None, string='capacity'):
    plt.figure()
    node_labels = {}
    for a_node in graph.nodes:
        node_labels[a_node] = a_node.label
    edge_labels = dict([((u, v,), d[string])
                        for u, v, d in graph.edges(data=True)])
    print(edge_labels)
    if not pos:
        pos = networkx.nx_agraph.graphviz_layout(graph)

    #node colors,
    val_map = {'src': 1.0,
               'v': 0.5714285714285714,
               'sink': 0.0,
               'root':0.7,
               's': 0.7,
               't': 0.25
               }

    # Assign colors to nodes
    values = []
    for a_node in graph.nodes:
        for a_key in val_map.keys():
            if a_key in a_node.label:
                values.append(val_map[a_key])
                break

    # values = [val_map.get(node, 0.45) for node in graph.nodes()]


    networkx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels)
    networkx.draw(graph, pos, node_color=values, node_size=1000, edge_color='r', edge_cmap=plt.cm.Reds, labels=node_labels)
    pylab.show()
    plt.show()

# Example

def get_a_directed_graph():
    graph = networkx.DiGraph()
    nodes = []
    for i in range(1,10):
        nodes.append(Node("v{0}".format(i), i, i))

    # graph.add_nodes_from(nodes)

    for i in range(1, len(nodes)):
        graph.add_edge(nodes[i-1], nodes[i], capacity=i*10)
    return graph



# This example going to be source example to be worked in algorithm
def get_multi_src_sink_graph():
    graph = networkx.DiGraph()
    nodes = []
    for i in range(0, 12):
        nodes.append(Node("v{0}".format(i), 0))

    #Name srcs
    nodes[0].label = 'src0'
    nodes[1].label = 'src1'
    nodes[2].label = 'src2'
    nodes[3].label = 'src3'
    nodes[4].label = 'src4'

    # name sinks

    nodes[9].label = 'sink9'
    nodes[10].label = 'sink10'
    nodes[11].label = 'sink11'


    # set src capacities
    nodes[0].set_capacity(15)
    nodes[1].set_capacity(10)
    nodes[2].set_capacity(40)
    nodes[3].set_capacity(10)
    nodes[4].set_capacity(12)

    # set sink capacities
    nodes[9].set_capacity(-60)
    nodes[10].set_capacity(-10)
    nodes[11].set_capacity(-30)


    # add edges 14 in total
    graph.add_edge(nodes[0], nodes[5], capacity=10, flow=0)
    graph.add_edge(nodes[1], nodes[5], capacity=12, flow=0)

    graph.add_edge(nodes[1], nodes[6], capacity=5,flow=0)
    graph.add_edge(nodes[2], nodes[6], capacity=8,flow=0)

    graph.add_edge(nodes[2], nodes[7], capacity=14,flow=0)
    graph.add_edge(nodes[3], nodes[7], capacity=7,flow=0)

    graph.add_edge(nodes[3], nodes[8], capacity=11,flow=0)
    graph.add_edge(nodes[4], nodes[8], capacity=2,flow=0)

    graph.add_edge(nodes[5], nodes[9], capacity=3,flow=0)

    graph.add_edge(nodes[6], nodes[9], capacity=15,flow=0)

    graph.add_edge(nodes[6], nodes[10], capacity=6,flow=0)

    graph.add_edge(nodes[7], nodes[10], capacity=20,flow=0)

    graph.add_edge(nodes[7], nodes[11], capacity=13,flow=0)

    graph.add_edge(nodes[8], nodes[11], capacity=18,flow=0)

    return graph


def create_s_t_graph(graph):

    if graph:
        sources = []
        sinks   = []
        for a_node in graph.nodes:
            if a_node.capacity > 0:
                sources.append(a_node)

        for a_node in graph.nodes:
            if a_node.capacity < 0:
                sinks.append(a_node)

        graph_len = len(graph)
        s = Node('s', 99999)
        st_graph = networkx.DiGraph()
        for a_src in sources:
            st_graph.add_edge(s, a_src, capacity=a_src.capacity, flow=0)
            #a_src.capacity = 0

        t = Node('t', -99999)
        for a_sink in sinks:
            st_graph.add_edge(a_sink, t, capacity=-1*a_sink.capacity, flow=0)
            #a_sink.capacity = 0
        for a_edge in graph.edges:
            edge_data = graph.get_edge_data(a_edge[0], a_edge[1])
            st_graph.add_edge(a_edge[0], a_edge[1], capacity=edge_data['capacity'], flow=edge_data['flow'])

    return st_graph



def get_src(graph):
    src = None
    for a_node in graph.nodes:
        if a_node.label == "s":
            src = a_node
            break
    return  src

def get_sink(graph):
    sink = None
    for a_node in graph.nodes:
        if a_node.label == "t":
            sink = a_node
            break
    return sink


def get_max_flow_simple(graph):
    src = sink = None
    src = get_src(graph)
    sink = get_sink(graph)
    print(src.to_string())
    print(sink.to_string())
    flow_value, flow_dict = networkx.maximum_flow(graph, src, sink)
    print(flow_value)



# Edges will be in same or reverse direction
# if capacity
def get_residual_graph(graph):
    if isinstance(graph, networkx.DiGraph):

        res_graph = networkx.DiGraph()
        for a_edge in graph.edges():
            flow = graph.get_edge_data(a_edge[0], a_edge[1])
            if isinstance(flow, dict):
                print(flow)
            flow = flow['flow']
            capacity = graph.get_edge_data(a_edge[0], a_edge[1])['capacity']
            if flow > 0:
                res_graph.add_edge(a_edge[1], a_edge[0], weight=flow, tag='reverse', color='r')
            if capacity > flow:
                res_graph.add_edge(a_edge[0], a_edge[1], weight=capacity-flow, tag='feasible', color='g')
        return res_graph





def draw_residual_graph(graph, pos, val_map, figure):
    """
    :param graph: Input graph is residual graph
    :return:
    """
    plt.figure(figure)
    node_labels = {}
    for a_node in graph.nodes:
        node_labels[a_node] = a_node.label


    # Node colors:
    values = []
    for a_node in graph.nodes:
        for a_key in val_map.keys():
            if a_key in a_node.label:
                values.append(val_map[a_key])
                break

    # All feasible edges should of one color while reverse edges of another color.
    edge_labels = dict([((u, v,), d['weight'])
                        for u, v, d in graph.edges(data=True)])

    # Assign colors to nodes
    edge_colors = [graph[u][v]['color'] for u,v in graph.edges]
    if not pos:
        pos = networkx.nx_agraph.graphviz_layout(graph)

    networkx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels)
    networkx.draw(graph, pos, node_color=values, node_size=1000, edge_color=edge_colors, edge_cmap=plt.cm.Reds,
                  labels=node_labels)

    pylab.show()
    plt.show()

# This should always give you same pos
def get_pos(graph, pos):
    if not pos:
        pos = networkx.nx_agraph.graphviz_layout(graph)
    return pos




def main():
    graph_simple = get_multi_src_sink_graph()
    for a_edge in graph_simple.edges:
        print(graph_simple.get_edge_data(a_edge[0], a_edge[1]))
    graph_st = create_s_t_graph(graph_simple)
    pos = get_pos(graph_st, None)
    draw_a_graph(graph_st, 'r')
    graph_residual = get_residual_graph(graph_st)

    draw_a_graph(graph_residual)
    draw_residual_graph(graph_residual, pos, 'r')




def print_graph(graph):
    for a_node in graph.nodes:
        print("Node: {0}".format(a_node.label))