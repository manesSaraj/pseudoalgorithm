# This is implementation of :
# The Pseudoflow Algorithm: A New Algorithm
#       for the Maximum-Flow Problem
# Link to original reserach paper: https://pdfs.semanticscholar.org/67e1/5c4b288590f206b55156affd97f17355915d.pdf
import datetime

import networkx
from NetworkFlow import *
import matplotlib.pyplot as plt

def find_corresponding_node_in_tree(node_in_st, tree):
    for a_node in tree.nodes:
        if a_node.label == node_in_st.label:
            return a_node
    return None


def find_corresponding_node_in_st_graph(node_in_tree, st_graph):
    for a_node in st_graph.nodes:
        if a_node.label == node_in_tree.label:
            return a_node
    return None


def create_tree(st_graph):
    tree = networkx.Graph()
    root_node = Node("root", 9999) # 9999 is indicating infinity
    st_src = get_src(st_graph)
    st_sink = get_sink(st_graph)
    print("Lenght of st-graph is {0}".format(len(st_graph)))
    print_graph(st_graph)
    for a_node in st_graph.nodes:
        if a_node == st_sink or a_node == st_src:
            continue
        new_node = a_node.clone_self()
        tree.add_edge(root_node, new_node, excess=0)
        #print("Updated len of tree is {1}".format(len(tree)))
    print("Nodes in tree are {0}".format(len(tree)))
    # Now update strong nodes
    strong_nodes = {}   # Map of strong node in st to strong node in G extended
    for a_neighbor in st_graph[st_src]:
        edge_data = st_graph.get_edge_data(st_src, a_neighbor)
        if edge_data['flow'] > 0:
            tree_node = find_corresponding_node_in_tree(a_neighbor, tree)
            if tree_node:
                tree_branch_data = tree.get_edge_data(root_node, tree_node)
                tree_branch_data['excess'] = edge_data['flow']
                strong_nodes[a_neighbor] = tree_node


    # Now update sink nodes
    weak_nodes = {}
    for a_edge in st_graph.edges:
        if a_edge[1] == st_sink:
            edge_data = st_graph.get_edge_data(a_edge[0], a_edge[1])
            if edge_data['flow'] > 0:
                tree_node = find_corresponding_node_in_tree(a_edge[0], tree)
                if tree_node:
                    tree_branch_data = tree.get_edge_data(root_node, tree_node)
                    tree_branch_data['excess'] = -1 * edge_data['flow']
                    weak_nodes[a_edge[0]] = tree_node

    for a_neighbor in tree[root_node]:
        edge_data = tree.get_edge_data(root_node, a_neighbor)
        if edge_data['excess'] == 0:
            st_node = find_corresponding_node_in_st_graph(a_neighbor, st_graph)
            weak_nodes[st_node] = a_neighbor

    return (root_node, tree, strong_nodes, weak_nodes)

def initialize(st_graph):

    if isinstance(st_graph, networkx.DiGraph):
        for a_edge in st_graph.edges:
            edge_data = st_graph.get_edge_data(a_edge[0], a_edge[1])
            assert (edge_data['flow'] == 0)

        src_node = get_src(st_graph)
        sink_node = get_sink(st_graph)

        # Saturate src edges
        for a_neighbor in st_graph[src_node]:
            edge_data = st_graph.get_edge_data(src_node, a_neighbor)
            edge_data['flow'] = edge_data['capacity']

        # Saturate sink edges as well
        for a_edge in st_graph.edges:
            if a_edge[1] == sink_node:
                edge_data = st_graph.get_edge_data(a_edge[0], a_edge[1])
                edge_data['flow'] = edge_data['capacity']

        # Create Tree
        (root_of_tree, tree, strong_set, weak_set )= create_tree(st_graph)
    return (root_of_tree, tree, strong_set, weak_set)

def bfs(src, graph, sink_set):
    queue = []
    queue.append(src)
    visited = {}
    parent  = {}
    parent[src]  = src
    for a_node in graph.nodes:
        visited[a_node] = False
    visited[src] = True

    while queue:
        a_node = queue.pop(0)
        for a_neighbor in graph[a_node]:
            if visited[a_neighbor] ==False:
                queue.append(a_neighbor)
                visited[a_neighbor] = True
                parent[a_neighbor] = a_node

    is_sink_reachable = False
    sink_node = None
    if sink_set:
        for a_sink in sink_set.keys():
            for a_node in graph.nodes:
                if a_node.label == a_sink.label:
                    a_sink = a_node
                    break
            if visited[a_sink]:
                sink_node = a_sink
                is_sink_reachable = True
                break;
    return (is_sink_reachable, parent, sink_node)


def exaustive_bfs(graph, src_set, sink_set):
    for a_src in src_set.keys():
        (is_path, parent, sink) = bfs(a_src, graph, sink_set)
        if is_path:
            return (a_src, sink, parent)
    return (None, None, None)



def residual_flow(src_set, sink_set, st_graph):
    # TODO:
    residual_graph =get_residual_graph(st_graph)
    draw_residual_graph(residual_graph, pos=None, val_map=val_map, figure=1)
    # BFS find a path from src_set to sink set
    (src_node, sink_node, path) = exaustive_bfs(residual_graph, src_set, sink_set)
    if src_node and sink_node and path:
        return (True, src_node, sink_node, path, residual_graph)
    else:
        return (False, None, None, None, residual_graph)

def belong_to_different_set(node1, node2, src_set, sink_set):
    src_found = False
    sink_found = False
    for a_node in src_set.keys():
        if a_node.label == node2.label:
            src_found = True
    for a_node in sink_set.keys():
        if a_node.label == node1.label:
            sink_found = True
    return src_found and sink_found

def find_merger_edge(src, sink, path, residual_graph, strong_set, weak_set):
    itr_node = sink
    while path[itr_node] != itr_node and not belong_to_different_set(itr_node, path[itr_node], strong_set, weak_set) :
        itr_node = path[itr_node]
    if itr_node == path[itr_node]:
        return (None, None)
    return (path[itr_node], itr_node)

def split((u, v)):
    return None



def get_root_branch_node(tree, tree_root, a_branch_node):
    (is_sink_reachable, parent, sink_node) = bfs(tree_root, tree, None)
    if parent:
        iter_node = a_branch_node
        while parent[iter_node] != tree_root:
            iter_node = parent[iter_node]
        return iter_node
    return Node




def merge(tree, tree_root, u, v, path, residual_graph):

    ru = get_root_branch_node(tree, tree_root, u)
    rv = get_root_branch_node(tree, tree_root, v)
    excess_edge = tree.get_edge_data(tree_root, ru)
    if excess_edge:
        excess_amount = excess_edge['excess']
        assert excess_amount > 0, "Excess amount can't be negative"
        # TODO: Remove Edge from T
        tree.add_edge(u, v, excess=0)  # Does this will be zero ?
        tree.remove_edge(tree_root, ru)
        # Now we finding augment path
        (is_sink_reachable, path, sink_node) = bfs(ru, tree, None)  # all possible paths
        augment_path = []           # Only path we are interested in
        iter_node = tree_root
        while iter_node != ru:
            augment_path.append(iter_node)
            if iter_node == path[iter_node]:
                print("There is self loop in tree at node ", iter_node.label)
            iter_node = path[iter_node]

        # Update flow:
        for i in range(1, len(augment_path)):
            edge_capacity = tree.get_edge_data(augment_path[i-1], augment_path[i])['capacity']
            if edge_capacity < excess_amount:
                tree = split(tree, tree_root, augment_path[i-1], augment_path[i])
                # TODO More here
            else:
                flow += flow + excess_amount
    else:
        print("No edge from tree root to branch root of tree, can't merge")
    return tree

def update_strong_set(strong_set, tree, tree_root):
    for a_neighbor in tree[tree_root]:
        if a_neighbor in strong_set.keys():
            edge_data = tree.get_edge_data(tree_root, a_neighbor)['excess']
            if edge_data and edge_data > 0:
                continue
            else:
                strong_set.pop(a_neighbor)
    return strong_set

def update_weak_set(weak_set, tree, tree_root):
    for a_neighbor in tree[tree_root]:
        if a_neighbor in weak_set.keys():
            edge_data = tree.get_edge_data(tree_root, a_neighbor)['excess']
            if edge_data and edge_data <= 0:
                continue
            else:
                weak_set.pop(a_neighbor)
    return weak_set
pos = None
val_map = None
def generic_pseudoflow(st_graph, graph, posy, value_map):
    global pos
    global val_map
    pos = posy
    val_map = value_map
    if st_graph:
        (tree_root, tree, strong_node_set, weak_node_set) = initialize(st_graph)
        take_snap_shot(tree,pos, value_map)        # Print Tree
        (is_flow_possible, src, sink, path, residual_graph) \
            = residual_flow(strong_node_set, weak_node_set, graph)
        figure = 11
        while is_flow_possible:
            draw_residual_graph(residual_graph, pos, val_map=value_map, figure=figure)
            print_graph(residual_graph)
            figure += 1
            dump_info(src, sink, path, is_flow_possible)
            (src_s, sink_w) = find_merger_edge(src, sink, path,
                                               residual_graph,
                                               strong_node_set,
                                               weak_node_set)
            src_s_tree = find_corresponding_node_in_tree(src_s, tree)
            sink_w_tree = find_corresponding_node_in_tree(sink_w, tree)
            tree = merge(tree, tree_root, src_s_tree, sink_w_tree,
                         path, residual_graph)
            # tree, tree_root, (u, v), path, residual_graph
            strong_node_set = update_strong_set(strong_node_set)
            weak_node_set   = update_weak_set(weak_node_set)
            (is_flow_possible, src, sink, path, residual_graph) = residual_flow(strong_node_set, weak_node_set,
                                                                                st_graph)

        return strong_node_set
    return None




def take_snap_shot(graph, pos, value):
    try:
        draw_a_graph(graph, value, figure=datetime.datetime.now(), pos=pos, string='excess')
    except KeyError:
        draw_tree(graph, value)
def dump_info(nodes, nodesink, path, is_possible):
    print("Flow possible ", is_possible)
    print("Source Node is {0} and Sink Node is {1}".format(nodes.label, nodesink.label))
    print("Yo Yo")
    for a_key in path.keys():
        print("{0}-> {1}".format(a_key.label, path[a_key].label))

def draw_tree(tree, value):
    draw_a_graph(graph=tree, val_map=value, figure=1, pos=None, string='excess')
    return
    """
    plt.figure()
    node_labels = {}
    for a_node in tree.nodes:
        node_labels[a_node] = a_node.label
    edge_labels = dict([((u, v,), d['excess'])
                        for u, v, d in tree.edges(data=True)])
    networkx.draw_networkx(tree)
    """

def print_path(path):
    for a_key in path.keys():
        print("{0}->{1}".format(a_key, path[a_key]))