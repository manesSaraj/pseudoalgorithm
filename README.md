This is implementation of Algorithm for Paper: The Pseudoflow Algorithm: A New Algorithm for the Maximum-Flow Problem by Dorit S. Hochbaum.

Search technique used for finding augmenting path is very simple version of BFS. 
NetworkX package is used to plot graphs, but its little bit buggy (Maybe overall some more bugs will be there). 
This work was done for project report for CSI5163[W] Algorithm Analysis And Design 2019.

