
from PseudoFlowAlgorithm import *


# Starting algorithms and managing plots
def start_algorithm(graph_st, graph, pos, val_map):
    if graph_st:
        generic_pseudoflow(graph_st, graph, pos, val_map)

















def main():
    # colors to be used while drawing
    val_map = {'src': 1.0,
               'v': 0.5714285714285714,
               'sink': 0.0,
               's': 0.7,
               't': 0.25
               }


    figure = 0
    print("Running Pseudo flow algorithm")
    # get all graphs
    graph = get_multi_src_sink_graph()
    graph_st = create_s_t_graph(graph)
    residual_graph = get_residual_graph(graph_st)
    pos = get_pos(graph, None)
    # Draw initialial graph
    draw_a_graph(graph, val_map,figure, None)
    #pos = get_pos(graph_st, None)
    figure += 1
    #draw_a_graph(graph_st, 'r', figure, pos)
    figure += 1
    #draw_residual_graph(residual_graph, pos, val_map, figure)

    start_algorithm(graph_st, graph, pos, val_map)

main()